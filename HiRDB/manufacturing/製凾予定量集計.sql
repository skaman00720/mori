select
  cast(SUBSTR(CAST(製凾日 AS CHAR(8)),1,4) as decimal(4,0)) as 年,
  cast(SUBSTR(CAST(製凾日 AS CHAR(8)),5,2) as decimal(4,0)) as 月,
  cast(SUBSTR(CAST(製凾日 AS CHAR(8)),7,2)  as decimal(4,0)) as 日,
  ekany.機械コード,
  wkiki.機械名,
  sum(
    case
      when ekany.最終工程区分=1 then
        Int(
          (wsyhn.単品平米 * 仕上数量 )
        )
      else
        Int(
          ((仕上寸法巾 * 仕上寸法流れ) * 仕上数量 * 仕上Ａ取数 / 仕上Ｂ取数)
        / 1000000 + .5)
    end
  ) as 予定㎡

from
  MORIADM.ekanypp as ekany

join
  MORIADM.wkikipp as wkiki
  on ekany.機械コード=wkiki.機械コード

join
  moriadm.wsyhnpp as wsyhn
  on ekany.得意先コード=wsyhn.得意先コード
  and ekany.品名コード=wsyhn.品名コード

where
  ekany.品目区分 in (1,2)
  and
  (後工程コード=0 or 後工程コード>1000)
  and
  製凾実績数量=0

group by
  製凾日,ekany.機械コード,wkiki.機械名
