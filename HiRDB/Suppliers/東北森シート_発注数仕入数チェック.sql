/* 東北森シート_発注数仕入数チェック */

select
  agasi.仕入日,
  agasi.入庫実績日,
  agasi.外注先コード,
  wtori.略称取引先名 as 外注先名,
  agasi.外注整理ＮＯ,
  agasi.受注ＮＯ,
  agasi.得意先コード,
  agasi.品名コード,
  agaht.総発注数量,
  agaht.仕入数量計

from
  moriadm.agasipp as agasi

join
  moriadm.agahtpp as agaht
  on agasi.外注注文ＮＯ=agaht.外注注文ＮＯ

join
  moriadm.wtoripp as wtori
  on agasi.外注先コード=wtori.取引先コード

where
  wtori.略称取引先名 like '%東北森%'
  and agasi.発注寸法巾>0
  and 仕入日>=CAST(VARCHAR_FORMAT(CURRENT_DATE - 50 days, 'YYYYMMDD') as DECIMAL(8,0))
