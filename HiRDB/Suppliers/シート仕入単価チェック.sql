select
  SUBSTR(
    CAST(agasi.入庫実績日 as CHAR(8))
  ,5,2)
  ||'/'||
  SUBSTR(
    CAST(agasi.入庫実績日 as CHAR(8))
  ,7,2) as 入庫日,

  agasi.外注先コード,
  wtori.略称取引先名 as 外注先名,
  agasi.外注整理ＮＯ,
  case
    when agasi.フルートコード='1' then 'AF'
    when agasi.フルートコード='2' then 'BF'
    when agasi.フルートコード='3' then 'WF'
    when agasi.フルートコード='4' then 'CF'
  END as フルート,
  wtego.伝票貼合構成名||agasi.特殊加工名１||agasi.特殊加工名２||agasi.特殊加工名３ as 貼合構成,

  agasi.発注寸法巾 as 巾,
  agasi.発注寸法流 as 流,
  agasi.仕入数量 as 数量,
  agaht.仕入単価 as 発注単価,
  agasi.単価 as 仕入単価,
  agasi.単価-agaht.仕入単価 as 差異単価,
  round(
    (agasi.単価-agaht.仕入単価)*(agasi.発注寸法巾*agasi.発注寸法流/1000000)*agasi.訂正後仕入数量
  ,0) as 差異金額

from
  agasipp as agasi

join
  agahtpp as agaht
  on agasi.外注注文ＮＯ=agaht.外注注文ＮＯ

join
  wtoripp as wtori
  on agasi.外注先コード=wtori.取引先コード

join
  wtegopp as wtego
  on agasi.貼合構成コード=wtego.貼合構成コード


where
  agasi.発注寸法巾>0
  and agasi.単価>agaht.仕入単価
  and SUBSTR(CAST(agasi.仕入日 AS CHAR(8)),1,6)= VARCHAR_FORMAT(CURRENT_DATE, 'YYYYMM')
  and agaht.仕入単価>0
  and
  (
    agasi.特殊加工名１ not like '%ｶｯﾄﾃｰﾌﾟ%' or
    agasi.特殊加工名１ not like '%ｶｯﾄﾃｰﾌﾟ%' or
    agasi.特殊加工名１ not like '%ｶｯﾄﾃｰﾌﾟ%'
  )
  
