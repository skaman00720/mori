select * from 表名
 where 更新日  > CAST(VARCHAR_FORMAT(CURRENT_DATE - 10 days, 'YYYYMMDD') as DECIMAL(8,0))

※CURRENT_DATE
　→現在日付を取得しております。取得データの方は日付型になります。

※VARCHAR_FORMAT（[日付型],変換する形式）
　→日付型から数値型へのCASTが不可なため、一度YYYYMMDDで文字型に変換しております。

※ CAST([…] AS データ型)
　→文字型データを列「更新日」と同じデータ型に変換しています。
