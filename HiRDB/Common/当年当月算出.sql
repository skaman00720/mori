SELECT
  *

FROM
  UURIFPP

WHERE
  SUBSTR(
    CAST(売上日 AS CHAR(8)),1,6
    )
  =
  VARCHAR_FORMAT(CURRENT_DATE,'YYYYMM')

/*
・CAST(売上日 AS CHAR(8))で「売上日」をDECIMALからCHAR型に変換しています。

・SUBSTR('文字列',1,6)で文字列の１番目から6番目までを取得します。
　つまり売上日の年月を取得しています。

・VARCHAR_FORMAT(CURRENT_DATE,'YYYYMM') で現在日付を「201602」の形式

　の文字列に変換しています。
*/
