
/* 売上訂正時 納品書の代わりに画面上でチェック */
select
  納品書ＮＯ,
  納品書行ＮＯ,
  uurif.請求先コード,
  wtori.略称取引先名 as 請求先名,
  売上日,
  出荷日,
  case
    when 売上区分=2 then '返品'
    when 売上区分=3 then '単価訂正'
    when 売上区分=4 then '数量訂正'
    when 売上区分=5 then '訂正'
    when 売上区分=13 then '当月外単価訂正'
    when 売上区分=14 then '当月外数量訂正'
  else 'その他'
  end as 売上区分名,
  uurif.受注ＮＯ,
  uurif.得意先コード,
  uurif.品名コード,
  uurif.品名,
  uurif.売上数量,
  uurif.売上単価,
  uurif.売上金額,
  uurif.摘要
from
  uurifpp as uurif

join
  wtoripp as wtori
  on uurif.請求先コード=wtori.取引先コード

where
  売上区分!=1
  and
  uurif.登録日=CAST( VARCHAR_FORMAT(CURRENT_DATE , 'YYYYMMDD') as DECIMAL(8,0))
