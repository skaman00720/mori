select
  uurif.売上日,
  uurif.請求先コード,
  wtori.略称取引先名 as 請求先名,
  uurif.納品書ＮＯ,
  uurif.納品書行ＮＯ,
  uurif.受注ＮＯ,
  uurif.得意先コード,
  uurif.品名コード,
  uurif.品名,
  uurif.売上数量 as 数量,
  uurif.売上単価 as 単価,
  uurif.売上金額 as 金額,
  wtork.登録者名,
  uurif.登録日,
  uurif.登録時間

from
  uurifpp as uurif

join
  wtoripp as wtori
  on uurif.請求先コード=wtori.取引先コード

join
  wtorkpp as wtork
  on cast(uurif.登録者コード as decimal)=wtork.登録者コード１

WHERE
  cast(
    cast(uurif.登録日 as char(8))
    ||
    cast(uurif.登録時間 as CHAR(6))
  as DECIMAL(14,0))
  >=20160401010101
  and
  SUBSTR(CAST(売上日 AS CHAR(8)),1,6)
　　　　　　 = VARCHAR_FORMAT(CURRENT_DATE - 1 MONTH, 'YYYYMM')

order by
  納品書ＮＯ,納品書行ＮＯ
