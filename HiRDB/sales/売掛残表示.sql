select
  uurik.取引先コード,
  wtori.略称取引先名 as 取引先名,
  前月残,
  入金実績額,
  今月売上額,
  消費税,
  今月残
from
  moriadm.uurikpp as uurik

left outer join
  moriadm.wtoripp as wtori
  on uurik.取引先コード=wtori.取引先コード
