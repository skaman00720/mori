/*
作成日 2016/04/30
MRCSの売上帳にて払出売上が 製品その他・商品シート・商品ケース に含まれているのでわかりづらいと赤平所長より作成依頼があったため
*/
CREATE VIEW IWATE.売上帳表示
  AS


select
  SUBSTR(cast(売上日 as CHAR(8)),1,4) as 年,
  SUBSTR(cast(売上日 as CHAR(8)),5,2) as 月,
  SUM(売上金額) as 金額,
  sum(
    case
      when 売上商品区分!=7 and 売上商品区分!=10 and 売上商品区分!=11 then 売上金額
    else
      0
    end
  ) as その他除外金額,
  SUM(
    case
      when 売上区分 in (1,2,4,14) then 売上平米
    else
      0
    end)
   as 売上㎡,
/*
  wtori.担当者コード,
  wtork.登録者名 as 担当者名,
  uurif.受注先コード,
  wtori.略称取引先名 as 受注先名,
*/
  /* 売上区分大 */
  case
    when 売上商品区分 in(1,2,5,6,7,10,11) then '通常売上'
    when 売上商品区分 in(3,4,8,9) then '払出売上'
  end as 売上区分,

  /* 売上区分中 */
  case
    when 売上商品区分 in(1,2,4) then '製品'
    when 売上商品区分 in(3,5,6,7,8,9) then '商品'
    when 売上商品区分=11 then '材料'
  end as 製商区分,

  /* 売上区分小 */
  case
    when 売上商品区分 in(1,3,5,8) then 'シート'
    when 売上商品区分 in(2,6,4,9) then 'ケース'
    when 売上商品区分 in(7,10) then 'その他'
    when 売上商品区分=11 then '材料'
  end as 詳細区分

from
  moriadm.uurifpp as uurif

/*
join
  moriadm.wtoripp as wtori
  on uurif.受注先コード=wtori.取引先コード

join
  moriadm.wtorkpp as wtork
  on wtori.担当者コード=wtork.登録者コード１
*/

where
  売上日 between
    cast(
      VARCHAR_FORMAT(
        (last_day(current_date - 2 month )+ 1 days ),'YYYYMMDD'
      ) as DECIMAL(8,0)
    )
    and
    cast(VARCHAR_FORMAT((current_date - 1 days),'YYYYMMDD') as DECIMAL(8,0))


group by
  売上日,売上商品区分
