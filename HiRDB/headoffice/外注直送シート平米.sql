select
  cast(SUBSTR(CAST(仕入日 AS CHAR(8)),1,4) as decimal(4,0)) as 年,
  cast(SUBSTR(CAST(仕入日 AS CHAR(8)),5,2) as decimal(4,0)) as 月,
  cast(SUBSTR(CAST(仕入日 AS CHAR(8)),7,2)  as decimal(4,0)) as 日,
  agasi.外注先コード,
  wtori.略称取引先名 as 納入先名,
  sum(
    round(
      発注寸法巾*発注寸法流/1000000*訂正後仕入数量,0
    )
  ) as 仕入㎡

from
  MORIADM.agasipp as agasi

left outer join
  MORIADM.wtoripp as wtori
  on agasi.納入場所=wtori.取引先コード

where
  agasi.納入形態=1
  and 発注寸法巾>0
  and 得意先コード>0
  and 仕入区分=1

group by
  仕入日,agasi.外注先コード,wtori.略称取引先名
