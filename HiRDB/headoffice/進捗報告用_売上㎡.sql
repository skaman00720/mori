
/* 進捗報告用 売上㎡ */
/* 本日の前日まで算出 */

select
  cast(SUBSTR(CAST(売上日 AS CHAR(8)),1,4) as decimal(4,0)) as 年,
  cast(SUBSTR(CAST(売上日 AS CHAR(8)),5,2) as decimal(4,0)) as 月,
  cast(SUBSTR(CAST(売上日 AS CHAR(8)),7,2)  as decimal(4,0)) as 日,
  sum(売上平米) as 売上㎡

from
  MORIADM.UURIFPP AS uurif

where
  売上日<=CAST(
    VARCHAR_FORMAT(CURRENT_DATE - 1 days, 'YYYYMMDD') as DECIMAL(8,0)
  )
  and
  売上商品区分 in (1,2,5,6)
  and
  売上区分 in (1,2,4,14)

group by
  売上日
