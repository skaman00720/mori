create view my_job.softbacketkei as


-- 前年からのソフトバケットの月別のソフトバケット数量確認

select
  year(売上日)-if(month(売上日)<4,1,0) as 年度,
  year(売上日) as 年,
  month(売上日) as 月,
  day(出荷日) as 日,
  uurif.品名コード,
  wsyhn.商品名,
  sum(訂正後売上数量) as 売上数量

from
  mori2stf.uurif as uurif

join
  mori1smf.wsyhn as wsyhn
  on uurif.得意先コード=wsyhn.得意先コード
  and uurif.品名コード=wsyhn.品名コード

where
  uurif.得意先コード=1001
  and 品名 like '%ソフトバケット%'

  AND IF((MONTH(NOW()) < 4),
  (CAST(uurif.売上日 AS DATE) >= CAST(CONCAT((YEAR(NOW()) - 3), '-04-01') AS DATE)),
  (CAST(uurif.売上日 AS DATE) >= CAST(CONCAT((YEAR(NOW()) - 2), '-04-01') AS DATE)))

  and 売上区分=1

group by
  出荷日,uurif.品名コード
