CREATE PROCEDURE `urihenpin` ()
BEGIN

-- -----------------------------------------------------
-- その他ロス用 売上返品㎡集計
-- -----------------------------------------------------
-- 作成日 2016/04/15
-- -----------------------------------------------------
-- 売上返品㎡がどのロスにもないので作成
-- 赤平所長リクエスト
-- -----------------------------------------------------



-- 不要テーブル削除
drop table if exists boss.urihenpin;

-- -----------------------------------------------------
create table boss.urihenpin as

select
  year(売上日)
    -if(month(売上日)<4,1,0)
    as 年度,
  year(売上日) as 年,
  month(売上日) as 月,
  uurif.受注先コード,
  wtori.取引先名 as 受注先名,
  sum(売上㎡) as 返品㎡

from
  mori2stf.uurif as uurif

join
  mori1smf.wtori as wtori
  on uurif.受注先コード=wtori.取引先コード


where
  売上日 between
    cast(
      concat(
        year(current_date())-
          if(month(current_date())<=4,5,4),
          '0401'
      ) as date)
    and
    last_day(date_sub(current_date(),interval 1 month))
  and
  売上区分=2
  and
  売上㎡<0

group by
  year(売上日),month(売上日),受注先コード

;
-- -----------------------------------------------------

alter table boss.urihenpin add primary key (年,月,受注先コード)
;

end
