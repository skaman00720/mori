CREATE DEFINER=`root`@`%` PROCEDURE `hibizaiko`()
BEGIN

-- 既存ビュー削除 --
drop view if exists commonjob.wseihv,commonjob.wseihv2,commonjob.wseihv3,commonjob.hibizaiko1;
drop table if exists commonjob.hibizaikokei,commonjob.hibizaikokei2;


-- 製品入出庫履歴ビュー作成 --
create view commonjob.wseihv as

select
入出庫日,
wtori.担当者コード,
sum(if(製品入出庫区分>20 and 後工程コード=0,round(入出庫数*wsyhn.単品平米,0),0) )as 入庫㎡,
sum(if(製品入出庫区分<20,round(入出庫数*wsyhn.単品平米,0),0) )as 出庫㎡


from mori2stf.wseih as wseih

inner join mori1smf.wkiju as wkiju
on wseih.入出庫日>=((wkiju.当月年月*100)+1)

inner join mori1smf.wsyhn as wsyhn
on wseih.得意先コード=wsyhn.得意先コード and wseih.品名コード=wsyhn.品名コード

inner join mori1smf.wtori as wtori
on wseih.得意先コード=wtori.取引先コード

where

在庫区分 =2
and

(製品入出庫区分=1 or 製品入出庫区分=2 or 製品入出庫区分=3 or 製品入出庫区分=4 or 製品入出庫区分=7 or 製品入出庫区分=9 or 製品入出庫区分=11 or
製品入出庫区分=21 or 製品入出庫区分=22 or 製品入出庫区分=23 or 製品入出庫区分=24 or 製品入出庫区分=27 or 製品入出庫区分=29)
and 倉庫コード=69999
and (wsyhn.品目区分=1 or wsyhn.品目区分=2)

group by 入出庫日,wtori.担当者コード

;


-- 入出庫㎡累計ビュー作成 --
create view commonjob.wseihv2 as

SELECT wseihv.*,
sum(ruikei.入庫㎡) as 入庫㎡累計,
sum(ruikei.出庫㎡) as 出庫㎡累計

FROM commonjob.wseihv as wseihv

left outer join commonjob.wseihv as ruikei
on wseihv.入出庫日>=ruikei.入出庫日 and wseihv.担当者コード=ruikei.担当者コード

group by wseihv.入出庫日,wseihv.担当者コード
;


-- 前月在庫平米総計ビュー作成 --
create view commonjob.wseihv3 as

select
wtori.担当者コード,
sum(round(前月残在庫数*wsyhn.単品平米,0)) as 前月在庫平米
from mori2stf.wsyhz as wsyhz

inner join mori1smf.wsyhn as wsyhn
on wsyhz.得意先コード=wsyhn.得意先コード and wsyhz.品名コード=wsyhn.品名コード

inner join mori1smf.wtori as wtori
on wsyhz.得意先コード=wtori.取引先コード

where
倉庫コード=69999 and 売商区分=2

group by wtori.担当者コード
;


-- 入出庫履歴 + 前月在庫総計 --

create table commonjob.hibizaikokei as


SELECT
date_format(wseih.入出庫日,'%Y/%m/%d') as 入出庫日,
wseih.担当者コード,
wtork.登録者名 as 担当者名,
wseih.入庫㎡累計,
wseih.出庫㎡累計,
zaiko.前月在庫平米,
zaiko.前月在庫平米+入庫㎡累計-出庫㎡累計 as 在庫㎡

FROM commonjob.wseihv2 as wseih

left outer join commonjob.wseihv3 as zaiko
on wseih.担当者コード=zaiko.担当者コード

left outer join mori1smf.wtork as wtork
on wseih.担当者コード=wtork.登録者CD
;


-- 最終日取得ビュー --
create view commonjob.hibizaiko1 as

SELECT
max(入出庫日) as 最終入出庫日
FROM commonjob.hibizaikokei as kei1

where 入出庫日<=date_sub(current_date , interval 1 day)
;


-- 最終日のみ担当別在庫テーブル作成 --
create table commonjob.hibizaikokei2

select kei.*
from commonjob.hibizaikokei as kei

inner join commonjob.hibizaiko1 as hiduke
on kei.入出庫日=hiduke.最終入出庫日
;

-- ビュー削除 --


drop view if exists commonjob.wseihv,commonjob.wseihv2,commonjob.wseihv3,commonjob.hibizaiko1;


END
