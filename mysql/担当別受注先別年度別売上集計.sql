create table moriwork.m160427 as 

select
  year(売上日)-if(month(売上日)<4,1,0) as 年度,
  uurif.担当者コード,
  wtork.登録者名 as 担当者名,
  uurif.受注先コード,
  wtori.取引先名 as 受注先名,
  sum(
    if(売上区分 in (1,2,4,14), 売上平米,0)
  ) as 売上㎡
from
  mori2stf.uurif as uurif

join
  mori1smf.wtori as wtori
  on uurif.受注先コード=wtori.取引先コード

join
  mori1smf.wtork as wtork
  on wtori.担当者コード=wtork.登録者CD


where
  売上日 BETWEEN 20110401 and 20160331
  and 売上商品区分 in(1,2,5,6)

group by
  year(売上日)-if(month(売上日)<4,1,0),
  
  uurif.受注先コード
