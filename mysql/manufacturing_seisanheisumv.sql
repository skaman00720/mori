CREATE
    ALGORITHM = UNDEFINED
    DEFINER = `root`@`%`
    SQL SECURITY DEFINER
VIEW `manufacturing`.`seisanheisumv` AS
    SELECT
        IF((MONTH(`mori2stf`.`ekanj`.`製凾実績日`) < 4),
            (YEAR(`mori2stf`.`ekanj`.`製凾実績日`) - 1),
            YEAR(`mori2stf`.`ekanj`.`製凾実績日`)) AS `年度`,
        YEAR(`mori2stf`.`ekanj`.`製凾実績日`) AS `年`,
        MONTH(`mori2stf`.`ekanj`.`製凾実績日`) AS `月`,
        DAYOFMONTH(`mori2stf`.`ekanj`.`製凾実績日`) AS `日`,
        `mori2stf`.`ekanj`.`機械コード` AS `機械コード`,
        `mori1smf`.`wkiki`.`機械名` AS `機械名`,
        SUM(`mori2stf`.`ekanj`.`生産平米`) AS `製凾㎡`
    FROM
        ((`mori2stf`.`ekanj`
        LEFT JOIN `mori1smf`.`wsyhn` ON (((`mori2stf`.`ekanj`.`得意先コード` = `mori1smf`.`wsyhn`.`得意先コード`)
            AND (`mori2stf`.`ekanj`.`品名コード` = `mori1smf`.`wsyhn`.`品名コード`))))
        LEFT JOIN `mori1smf`.`wkiki` ON ((`mori2stf`.`ekanj`.`機械コード` = `mori1smf`.`wkiki`.`機械コード`)))
    WHERE
        (((`mori2stf`.`ekanj`.`後工程コード` = 0)
            OR (`mori2stf`.`ekanj`.`後工程コード` > 1001))
            AND (`mori1smf`.`wsyhn`.`品目区分` BETWEEN 1 AND 2)
            AND IF((MONTH(NOW()) < 4),
            (CAST(`mori2stf`.`ekanj`.`製凾実績日` AS DATE) >= CAST(CONCAT((YEAR(NOW()) - 2), '-04-01') AS DATE)),
            (CAST(`mori2stf`.`ekanj`.`製凾実績日` AS DATE) >= CAST(CONCAT((YEAR(NOW()) - 1), '-04-01') AS DATE))))
    GROUP BY YEAR(`mori2stf`.`ekanj`.`製凾実績日`) , MONTH(`mori2stf`.`ekanj`.`製凾実績日`) , DAYOFMONTH(`mori2stf`.`ekanj`.`製凾実績日`) , `mori2stf`.`ekanj`.`機械コード`
