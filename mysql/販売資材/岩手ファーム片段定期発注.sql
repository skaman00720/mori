CREATE PROCEDURE `katadanteiki` ()
BEGIN

-- 既存データ削除
truncate table my_job.katadanteiki
;

-- 新規レコード追加
insert into
  my_job.katadanteiki(
    受注先コード,
    得意先コード,
    品名コード,
    品名,
    数量,
    納期,
    納入先コード,
    納入先名
  )

values
  (
    2329,
    1001,
    1431,
    '片段２５０×４２５',
    4000,
    date_add(last_day(current_date()), interval 1 day),
    2329,
    '（株）岩手ファーム本社パッキングセンター'
  ),

  (
    2329,
    1001,
    1431,
    '片段２５０×４２５',
    4000,
    date_add(last_day(current_date()), interval 15 day),
    2329,
    '（株）岩手ファーム本社パッキングセンター'
  )
;

END
