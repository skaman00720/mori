CREATE DEFINER=`root`@`%` PROCEDURE `sheetfuryo`()
BEGIN

-- --------------------------------------------------------------------------------
-- 変更履歴
-- 2015/02/24 東北森のシート不足 マイナスも表記 仕入数<通し枚数
-- 2015/11/10 シート不足内訳追加
-- --------------------------------------------------------------------------------



-- --------------------------------------------------------------------------------
-- ビュー テーブル 削除
-- --------------------------------------------------------------------------------
drop view if exists my_job.sheetfuryo1,my_job.sheetfuryo2,my_job.hachisaisyu;
drop table if exists my_job.sheetfuryo,my_job.sfuryo_hachi,my_job.sfuryo_senda
;


-- --------------------------------------------------------------------------------
-- シート不良元データ作成
-- --------------------------------------------------------------------------------

create view my_job.sheetfuryo1 as

select
DISTINCT
min(agasi.仕入日) as 仕入日,
agasi.外注先コード,
ekanj.製凾実績日,
ekanj.機械コード,
ekanj.工順,
ekanj.受注先コード,
juchu.取引先名 as 受注先名,
ekanj.受注ＮＯ,
agasi.外注整理ＮＯ as 整理ＮＯ,
ekanj.得意先コード,
ekanj.品名コード,
agasi.品名,
agasi.得意先コード as シート得意先コード,
agasi.品名コード as シート品名コード,
case
	when ekanj.他工程機械コード1=210 then 'そり'
	when ekanj.他工程機械コード1=310 then '傷'
	when ekanj.他工程機械コード1=320 then '水濡れ'
	when ekanj.他工程機械コード1=410 then '罫割れ'
	when ekanj.他工程機械コード1=110 then '汚れ'
	when ekanj.他工程機械コード1=280 then 'シワ'
else ' '
end as 不良内訳,
sum(ekanj.他工程不良品数1) as 他工程不良品数,
truncate(sum(ekanj.他工程不良品数1) /wkoju.通し取数Ｂ/agasi.取数,0) as 不良数,
sum(ekanj.生産枚数) as 生産枚数,
agasi.発注寸法巾,
agasi.発注寸法流,
agasi.フルートコード,
agasi.貼合構成コード,
if(agasi.特殊加工名１ is null , ' ', agasi.特殊加工名１) as 特殊加工名１,
if(agasi.特殊加工名２ is null , ' ', agasi.特殊加工名２) as 特殊加工名２,
if(agasi.特殊加工名３ is null , ' ', agasi.特殊加工名３) as 特殊加工名３,
agasi.取数 as シート取数,
agasi.訂正後単価,
sum(agasi.訂正後仕入数量) as シート仕入数,
sum(agasi.訂正後仕入数量)-truncate((sum(ekanj.生産枚数)/agasi.取数),0) as シート不足数


from mori2stf.ekanj as ekanj

left outer join mori1smf.wsyhk as wsyhk
on ekanj.得意先コード=wsyhk.親得意先コード and ekanj.品名コード=wsyhk.親品名コード and wsyhk.品目区分=4

left outer join mori2stf.agasi as agasi
on ekanj.受注ＮＯ=agasi.受注ＮＯ and wsyhk.子得意先コード=agasi.得意先コード and wsyhk.子品名コード=agasi.品名コード and agasi.仕入区分=1

inner join mori1smf.wtori as juchu
on ekanj.受注先コード=juchu.取引先コード

inner join mori1smf.wtori as gaichu
on agasi.外注先コード=gaichu.取引先コード

left outer join mori1smf.wkoju as wkoju
on ekanj.得意先コード=wkoju.得意先コード and ekanj.品名コード=wkoju.品名コード and ekanj.工順=wkoju.工順 and ekanj.機械コード=wkoju.工程コード

where
製凾実績日>last_day(date_sub(curdate(),interval 1 month))
and ekanj.前工程コード>1000 and ekanj.機械コード!=325

group by ekanj.機械コード,ekanj.受注ＮＯ,ekanj.得意先コード,ekanj.品名コード


/* 不良内訳名 他工程機械コード1
210:そり
310:きず
320:水濡れ
410:罫割れ
110:汚れ
280:シワ
*/

/*シート不足数算出
シート仕入数-(通し枚数*シート取数)
*/
;


-- --------------------------------------------------------------------------------
-- 仙台 八戸 共通 シート不良提出用データ作成
-- --------------------------------------------------------------------------------
create table my_job.sheetfuryo as

SELECT
date_format(仕入日,'%Y/%m/%d') as 仕入日,
外注先コード,
wtori.取引先名,
date_format(製凾実績日,'%Y/%m/%d') as 製凾日,
year(製凾実績日) as 製凾年,
month(製凾実績日) as 製凾月,
furyo.機械コード,
wkiki.機械名,
furyo.工順,
受注ＮＯ,
整理ＮＯ,
furyo.得意先コード,
furyo.品名コード,
if(受注先名 like '%東北森%' , 品名 ,
concat(
	case
		when フルートコード=1 then 'AF'
		when フルートコード=2 then 'BF'
		when フルートコード=3 then 'WF'
    when フルートコード=4 then 'CF'
	end,' ',
	wtego.伝票貼合構成名,
	特殊加工名１,' ',特殊加工名２,' ',特殊加工名３
)
) as 貼合構成,
発注寸法巾 as 巾,
発注寸法流 as 流,
訂正後単価 as 単価,

-- 不良
不良内訳,
if(不良数>0,不良数,0) as 不良数,
if(不良数>0,truncate(((発注寸法巾*発注寸法流)/1000000)*不良数,0),0) as 不良㎡,
if(不良数>0,truncate(((発注寸法巾*発注寸法流)/1000000)*不良数*訂正後単価,0),0) as 不良金額,

-- 不足
if(シート不足数>0,シート不足数,0) as 不足数,
if(シート不足数>0,truncate(((発注寸法巾*発注寸法流)/1000000)*シート不足数,0),0) as 不足㎡,
if(シート不足数>0,truncate(((発注寸法巾*発注寸法流)/1000000)*シート不足数*訂正後単価,0),0) as 不足金額,

-- 不良 + 不足
if(不良数>0,truncate(((発注寸法巾*発注寸法流)/1000000)*不良数,0),0)+if(シート不足数>0,truncate(((発注寸法巾*発注寸法流)/1000000)*シート不足数,0),0) as ㎡計,
if(不良数>0,truncate(((発注寸法巾*発注寸法流)/1000000)*不良数*訂正後単価,0),0) + if(シート不足数>0,truncate(((発注寸法巾*発注寸法流)/1000000)*シート不足数*訂正後単価,0),0) as 金額計,
fusoku.内訳名 as 不足内訳,

'' as 摘要

FROM my_job.sheetfuryo1 as furyo

inner join mori1smf.wtori as wtori
on furyo.外注先コード=wtori.取引先コード

inner join mori1smf.wkiki as wkiki
on furyo.機械コード=wkiki.機械コード

inner join mori1smf.wtego as wtego
on furyo.貼合構成コード=wtego.貼合CD

left outer join manufacturing.sheet_shortage as fusoku
on
	furyo.受注ＮＯ=fusoku.受注NO and
	furyo.機械コード=fusoku.機械コード and
	furyo.得意先コード=fusoku.得意先コード and
	furyo.品名コード=fusoku.品名コード and
	furyo.外注先コード=fusoku.前工程コード

/* 2015/02/24 東北森の抽出条件 仕入数<製造数  も含めるためwhere句をコメント化
where
不良数>0 or シート不足数>0
*/

order by 仕入日,外注先コード
;

-- --------------------------------------------------------------------------------
-- 仙台 八戸 共通 インデックス作成 属性変更
-- --------------------------------------------------------------------------------
create index 仕入日 on my_job.sheetfuryo(仕入日);
create index 外注先コード on my_job.sheetfuryo(外注先コード);
create index 製凾日 on my_job.sheetfuryo(製凾日);
create index 機械コード on my_job.sheetfuryo(機械コード);
create index 受注ＮＯ on my_job.sheetfuryo(受注ＮＯ);
create index 整理ＮＯ on my_job.sheetfuryo(整理ＮＯ);
create index 得意先コード on my_job.sheetfuryo(得意先コード);
create index 品名コード on my_job.sheetfuryo(品名コード);

ALTER TABLE `my_job`.`sheetfuryo` CHANGE COLUMN `仕入日` `仕入日` DATE;
ALTER TABLE `my_job`.`sheetfuryo` CHANGE COLUMN `製凾日` `製凾日` DATE;

/* 2015/11/10 ストアドエラーのためコメント化
alter table my_job.sheetfuryo modify 仕入日 date;
alter table my_job.sheetfuryo modify 製凾日 date;
*/


-- --------------------------------------------------------------------------------
-- 八戸のみテーブル作成
-- --------------------------------------------------------------------------------
create table my_job.sfuryo_hachi

SELECT * FROM my_job.sheetfuryo as furyo

inner join saiban.tohoku_henpin as henpin
on furyo.製凾日>henpin.最終抽出日

where 取引先名 like '%東北森%'
-- 2015/02/24 東北森の場合 シート数<通し枚数 の条件も含めるため以下の条件を追加

and (不良数!=0 or 不足数!=0)
order by 仕入日
;

-- --------------------------------------------------------------------------------
-- 八戸のみ インデックス作成
-- --------------------------------------------------------------------------------
create index 仕入日 on my_job.sfuryo_hachi(仕入日);
create index 外注先コード on my_job.sfuryo_hachi(外注先コード);
create index 製凾日 on my_job.sfuryo_hachi(製凾日);
create index 機械コード on my_job.sfuryo_hachi(機械コード);
create index 受注ＮＯ on my_job.sfuryo_hachi(受注ＮＯ);
create index 整理ＮＯ on my_job.sfuryo_hachi(整理ＮＯ);
create index 得意先コード on my_job.sfuryo_hachi(得意先コード);
create index 品名コード on my_job.sfuryo_hachi(品名コード);


-- --------------------------------------------------------------------------------
-- 八戸 最終仕入日データ更新
-- --------------------------------------------------------------------------------

-- 更新用ビュー作成
if dayofweek(current_date)=5 then

create view my_job.hachisaisyu as
SELECT
1 as id,
max(仕入日) as 最終仕入日
FROM my_job.sfuryo_hachi;

end if
;
-- 最終仕入日データ更新
if dayofweek(current_date)=5 then

update saiban.tohoku_henpin as saiban2 ,my_job.hachisaisyu as hachi
set saiban2.最終抽出日 = hachi.最終仕入日

where
saiban2.id=hachi.id;

end if;
-- ビュー削除
if dayofweek(current_date)=5 then
drop view if exists my_job.hachisaisyu;
end if;

-- --------------------------------------------------------------------------------
-- 仙台のみテーブ作成
-- --------------------------------------------------------------------------------
create table my_job.sfuryo_senda
SELECT * FROM my_job.sheetfuryo

where 取引先名 like '%仙台森%' and year(仕入日)=year(current_date()) and month(仕入日)=month(current_date())
 and (不良数>0 or 不足数>0)
order by 仕入日
;

-- --------------------------------------------------------------------------------
-- 仙台のみ インデックス作成
-- --------------------------------------------------------------------------------
create index 仕入日 on my_job.sfuryo_senda(仕入日);
create index 外注先コード on my_job.sfuryo_senda(外注先コード);
create index 製凾日 on my_job.sfuryo_senda(製凾日);
create index 機械コード on my_job.sfuryo_senda(機械コード);
create index 受注ＮＯ on my_job.sfuryo_senda(受注ＮＯ);
create index 整理ＮＯ on my_job.sfuryo_senda(整理ＮＯ);
create index 得意先コード on my_job.sfuryo_senda(得意先コード);
create index 品名コード on my_job.sfuryo_senda(品名コード);


-- --------------------------------------------------------------------------------
-- ビュー削除
-- --------------------------------------------------------------------------------
drop view my_job.sheetfuryo1;

END
