CREATE DEFINER=`root`@`%` PROCEDURE `tohokutokune`()
BEGIN

-- --------------------------------------------------------------------------------
--  東北森紙業へ定期的に報告する貼合構成寸法特値リスト作成
-- --------------------------------------------------------------------------------


-- --------------------------------------------------------------------------------
-- 変更履歴
-- --------------------------------------------------------------------------------
-- 2016/04/13
-- 東北森と仙台森の一般単価が変更になるため特値単価も仙台森と東北森の単価が変わる
-- 特値単価->東北森特値単価 に変更
-- --------------------------------------------------------------------------------


-- --------------------------------------------------------------------------------
-- 不要テーブル削除
-- --------------------------------------------------------------------------------
drop table if EXISTS my_job.tohokutokune1,tohokutokune
;


-- --------------------------------------------------------------------------------
-- 仕入データ 東北森一般のみ抽出
-- --------------------------------------------------------------------------------
create table my_job.tohokutokune1 as

SELECT
cast(仕入日 as date) as 仕入日,
受注先コード,
外注整理ＮＯ,
case
	when agasi.フルートコード=1 then 'AF'
	when agasi.フルートコード=2 then 'BF'
	when agasi.フルートコード=3 then 'WF'
END as フルート,
agasi.貼合構成コード,
伝票貼合構成名 as 貼合構成,
agasi.発注寸法巾 as 寸法巾,
agasi.発注寸法流 as 寸法流,
sum(agasi.訂正後仕入数量) as 仕入数,
agasi.訂正後単価 as 仕入単価,
wsyhn.撥水区分 as 特殊加工区分

from mori2stf.agasi as agasi

inner join mori1smf.wtori as wtori
on agasi.受注先コード=wtori.取引先コード

inner join mori1smf.wsyhn as wsyhn
on agasi.得意先コード=wsyhn.得意先コード and agasi.品名コード=wsyhn.品名コード

inner join mori1smf.wtego as wtego
on agasi.貼合構成コード=wtego.貼合CD

where agasi.外注先コード=20005 and 外注整理ＮＯ>0 and 仕入区分=1 and
仕入日 between last_day((curdate() - interval 1 month)) and (last_day(curdate()) - interval 1 day)

group by 外注整理ＮＯ
;

ALTER table my_job.tohokutokune1 add PRIMARY KEY (外注整理ＮＯ)
;


-- --------------------------------------------------------------------------------
-- 東北森提出用テーブル
-- --------------------------------------------------------------------------------
create table my_job.tohokutokune as

SELECT
	siire.仕入日,
	受注先コード,
	外注整理ＮＯ,
	貼合構成コード,
	concat(フルート,' ',貼合構成,' ',if(wtoka.特殊加工名 is null ,'',wtoka.特殊加工名)) as 貼合構成,
	寸法巾,
	寸法流,
	仕入数,
	仕入単価,

東北森特値単価 as 特値単価,
東北森特値単価-仕入単価 as 差異単価,
round((寸法巾*寸法流)/1000000*仕入数*(東北森特値単価-仕入単価),0) as 差異金額



FROM my_job.tohokutokune1 as siire

inner join morimaster.sendaitokune as tokune
on siire.受注先コード=tokune.得意先コード and siire.貼合構成コード=tokune.貼合コード and siire.寸法巾=tokune.製板指示巾 and siire.寸法流=tokune.製板指示流れ
and siire.特殊加工区分=tokune.特殊加工コード1

left outer join mori1smf.wtoka as wtoka
on siire.特殊加工区分=wtoka.特加区

-- 2016/03/08 仙台森の標準単価算出用
join mori1smf.wstta as wstta
	on siire.貼合構成コード=wstta.貼合コード
	and 14001=wstta.取引先コード

;

ALTER table my_job.tohokutokune add PRIMARY key (外注整理ＮＯ)
;

drop table if EXISTS my_job.tohokutokune1
;

END
